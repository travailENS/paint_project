from tkinter import *
import serial
from tkinter.colorchooser import askcolor
from tkinter import filedialog
from PIL import ImageGrab
 
ser = serial.Serial("COM5",baudrate=115200, timeout=0.02)
print(ser)

 
class Paint():
    
    DEFAULT_PEN_SIZE = 5.0
    DEFAULT_COLOR = 'black'
    def __init__(self,root):
         
        self.root = root
        self.root.geometry("1080x612")
        self.root.configure(background = "white")
        self.root.resizable(0,0)
        self.root.title("Paint - made in Python")
        
        #Définition fenêtre
        self.c = Canvas(self.root, bg='white', width=1080, height=612)
        self.c.grid(row=1, columnspan=5)
         
         #Choix outils
        self.pen_button = Button(self.root, text='pen', command=self.use_pen)
        self.pen_button.grid(row=0, column=0)

        self.color_button = Button(self.root, text='color', command=self.choose_color)
        self.color_button.grid(row=0, column=1)

        self.eraser_button = Button(self.root, text='eraser', command=self.use_eraser)
        self.eraser_button.grid(row=0, column=2)
        
        self.clear_button  = Button(self.root, text = 'Clear', command = lambda : self.c.delete("all"))
        self.clear_button.grid( row = 0, column = 3)
         
        self.save_button   = Button(self.root, text = 'Save', command = self.save_image)
        self.save_button.grid(  row = 0, column = 4)
      
        self.c.bind('<B1-Motion>', self.cadre)
          
        #Paramètre initiaux
        self.x     = 0
        self.y     = 0
        self.old_xrect = 0
        self.old_yrect = 0
        self.zoom  = 1
        self.old_zoom = 1
        
        self.line_width = 1
        self.color = self.DEFAULT_COLOR
        self.eraser_on = False
        self.active_button = self.pen_button
        
        self.root.after(10, self.paint)
     
    def use_pen(self):
        self.activate_button(self.pen_button)

    def choose_color(self):
        self.eraser_on = False
        self.color = askcolor(color=self.color)[1]

    def use_eraser(self):
        self.activate_button(self.eraser_button, eraser_mode=True)

    #Design bouton
    def activate_button(self, some_button, eraser_mode=False):
        self.active_button.config(relief=RAISED)
        some_button.config(relief=SUNKEN)
        self.active_button = some_button
        self.eraser_on = eraser_mode
             
    def paint(self):
         
        recu = ser.read(15)          # read 15 bytes

        if ((recu!=b'') and (recu[0]==40) and (len(recu)==15)) :
            print(recu)
            decodage   = recu.decode('utf-8')
           
            self.x     = int(decodage[1:4])
            self.y     = int(decodage[5:8])
            self.line_width = int(decodage[9:11])
            self.zoom  = int(decodage[12:14])

            if(self.old_zoom == self.zoom):
                x1 = (self.x - self.line_width ) * (10-self.zoom)/4 + self.old_xrect
                y1 = (self.y - self.line_width ) * (10-self.zoom)/4 + self.old_yrect
             
                x2 = (self.x + self.line_width ) * (10-self.zoom)/4 + self.old_xrect
                y2 = (self.y + self.line_width ) * (10-self.zoom)/4 + self.old_yrect 
                 
                paint_color = 'white' if self.eraser_on else self.color
                self.c.create_oval(x1, y1, x2, y2, fill=paint_color,
                                   outline = paint_color)
        self.root.after(10, self.paint)

    def cadre(self,event):
        if (self.old_xrect!=event.x) or (self.old_yrect !=event.y) or (self.old_zoom != self.zoom):
            self.c.create_rectangle(self.old_xrect , self.old_yrect,self.old_xrect + 120 * (10-self.old_zoom) + 2 ,
                                    self.old_yrect +  68 * (10-self.old_zoom) + 2,
                                    width = 2, outline = 'white')
            self.c.create_rectangle(event.x , event.y, event.x + 120 * (10-self.zoom)+2,event.y +  68 * (10-self.zoom)+2,width = 2, outline = 'red')
        self.old_xrect = event.x
        self.old_yrect = event.y
        self.old_zoom = self.zoom
     
    def save_image(self):
        try :
            filename = filedialog.asksaveasfilename(defaultextension = '.jpg')
            x = Canvas.winfo_rootx(self.c)
            y = Canvas.winfo_rooty(self.c)
            img = ImageGrab.grab((x+2,y+2,x + 1078,y+580)).save(filename)
            messagebox.showinfo('Success','Image successfully saved as ' + str(filename))
        except :
            messagebox.showerror('Error','Unable to save')
 
  
if __name__ == "__main__":
   root = Tk()
   p = Paint(root)
   root.mainloop()

ser.close()
