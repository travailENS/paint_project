# Liaison serie

Pour récupérer l'endroit où l'utilisateur a touché, on réutilise le protocole de récupération de la position de touche :

```javascript
BSP_TS_GetState(&TS_State);
if(TS_State.touchDetected){
    x0 = TS_State.touchX[0];
	y0 = TS_State.touchY[0];
    ...}
```
On configure la liaison série huart1 à 115 00 bauds, 8 bits envoyés, pas de parité et 1 bit de stop. On envoie ensuite la position de la touche si elle est différente par rapport à la précendente :
```javascript
if((x!=x0)||(y!=y0)))
    {sprintf(envoi_position,"(%3u,%3u)",x,y);
    HAL_UART_Transmit(&huart1, envoi_position, 9, 20);}
```
On ajoute aussi la visualisation du curseur rond sur la carte :

![Curseur rond](./images/IMG_20210420_134724.jpg)

Après avoir vérifié que l'on reçoit bien via le terminal Teraterm, il s'agit maintenant de la récupérer les 9 bytes envoyés via Python:
```python
import serial

#Ouverture du port série
ser = serial.Serial("COM5",baudrate=115200, timeout=0.01, writeTimeout=0.1)

#Lecture de 9 bytes par 9 bytes
recu = ser.read(9) 
```
