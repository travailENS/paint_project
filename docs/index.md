# Présentation

L'idée générale est de recréer une tablette grahique où l'utilisateur dessine sur l'écran tactile de la STM32f7-discovery et le résultat s'affiche sur une interface graphique à l'ordinateur. 

<center>
<video width="500" autoplay muted>
<source src=./images/VID_20210420_141902.mp4 type=video/mp4>
</video>
</center>
Vous trouverez le lien du dossier git au lien [suivant](https://gitlab.com/travailENS/paint_project.git).

##### Les différentes tâches réalisées dans ce projet sont dans l'ordre :
* Envoi de la position depuis la STM32f7-discover par liaison série
* Récupération et traitement des données par Python
* Création de l'interface graphique avec Tkinter
* Dessin "en temps réel" sur le canvas
* Gestion du zoom et modification de la zone de dessin

Ce site détaille les différentes étapes effectuées.



_Réalisé par Camille HERLENT_


