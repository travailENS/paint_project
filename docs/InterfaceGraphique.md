# Interface Python

Pour cela, j'ai utilisé le module Tkinter qui permet d'avoir une GUI (_Graphic User Interface_) qui, contrairement à un programme console 'classique' qui se bloque en attendant une action de l'utilisateur, va boucler en surveillant en permanence s'il y a eu un événement et en fontion de cela effectuer certaines tâches.


![Tkinter](./images/Tkinter_schema.PNG)

## Création d'une fênetre Tkinter
Pour créer une fenêtre basique, la procédure est :
```python
from tkinter import *

# fenêtre principale
fenPrincipale = Tk()

# boucle de surveillance des événements
fenPrincipale.mainloop()
```
On obtient la fenêtre suivante :
![Fenêtre simple](./images/Tkinter_fenetre_sans_rien.PNG)

## Mise en place du Paint

Je me suis basée sur le [modèle suivant](#https://gist.github.com/nikhilkumarsingh/85501ee2c3d8c0cfa9d1a27be5781f06
). L'utilisation de la programmation objet est indispensable dans notre cas. J'ai adapté le code avec les éléments suivants :
* Bouton 'Pen' pour dessiner
* Bouton 'Color' pour choisir la couleur grâce au module askcolor :
* Bouton 'Erase' pour effacer
* Bouton 'Clear' pour recommencer tout le dessin
* Bouton 'Save' que nous détaillerons plus tard

Le résultat est le suivant : 
![Choix couleurs](./images/colours.PNG)* Bouton

## Dessin "en temps réel"

On récupère les coordonnées envoyées par liaison série par paquets de bytes auxquelles on a ajouté la gestion de la taille de crayon via le 2eme potentiomètre de la cart discovery. On créé une fonction _paint_ qui va s'exécuter toutes les 10 ms, regarder si des données ont été transmises et si oui dessiner un rond sur le canvas :

```python
def paint(self): 

    recu = ser.read(15)          # read 15 bytes

    #On vérifie que la transmission est correcte
    if ((recu!=b'') and (recu[0]==40) and (len(recu)==15)) : 

        #Le résultat est envoyé sous format 'byte' il faut donc le décoder
        decodage   = recu.decode('utf-8') 
           
        self.x     = int(decodage[1:4])
        self.y     = int(decodage[5:8])
        self.line_width = int(decodage[9:11])

        x1 = (self.x - self.line_width )
        y1 = (self.y - self.line_width )      
        x2 = self.x + self.line_width  
        y2 = self.y + self.line_width 

        #Pour créer un rond situé dans le carré de coins (x1,y1) et (x2,y2)         
        paint_color = 'white' if self.eraser_on else self.color
                self.c.create_oval(x1, y1, x2, y2, fill=paint_color,
                                   outline = paint_color)
        #Pour redéclancher le timer et boucler                           
        self.root.after(10, self.paint)
```


## Ajout de la sauvegarde

Il était intéressant d'avoir une fonctionnalité sauvegarde afin de conserver l'oeuvre réalisée (pour après l'imprimer, la faire encadrer,...). Cela a été possible en ajoutant le module filedialog de Tkinter permettant d'ouvrir une boîte de dialogue d'enregistrement et le module ImageGrab qui fait une sélection de l'écran suivant les coordonnées du canvas :

```python
from tkinter import filedialog
from PIL import ImageGrab

...

def save_image(self):
    #Pour choisir le lieu et le nom de sauvegarde
    filename = filedialog.asksaveasfilename(defaultextension = '.jpg')

    #Copie de la fenêtre de dessin puis sauvegarde
    x = Canvas.winfo_rootx(self.c)
    y = Canvas.winfo_rooty(self.c)
    img = ImageGrab.grab((x+2,y+2,x + 1078,y+580)).save(filename)
```
On vérifie bien le fonctionnement de ce code :
![enregistrement](./images/Save.PNG)

## Gestion du zoom

La gestion du zoom a été la dernière fonctionnalité mise en place. Elle permet d'être plus précis et d'avoir une zone de dessin plus grande que les 480x272 pixels de la carte. Pour cela, l'utilisateur règle le zoom voulu via le 1er potentiomètre de la carte qui est récupéré par la liaison série puis il choisit l'emplacement de la zone de dessin avec un clic de souris. Un rectangle rouge apparaît alors pour délimiter la zone de dessin.

![smiley](./images/zoom_canvas.PNG)

Ce qui a été le plus complexe a été d'adapter la façon de dessiner en fonction de cette zone car justement elle doit délimiter là où on peut dessiner. Il a fallu donc modifier l'endroit de dessin mais aussi l'épaisseur du trait en fonction du zoom via une tranformation affine.

```python
x1 = (self.x - self.line_width ) * (10-self.zoom)/4 + self.old_xrect
y1 = (self.y - self.line_width ) * (10-self.zoom)/4 + self.old_yrect
             
x2 = (self.x + self.line_width ) * (10-self.zoom)/4 + self.old_xrect
y2 = (self.y + self.line_width ) * (10-self.zoom)/4 + self.old_yrect 
```





