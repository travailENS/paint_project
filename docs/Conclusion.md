# Conclusion de ce projet

## Pistes d'amélioration

Avec plus de temps il aurait été intéressant d'ajouter un système de calques qui est très utile en graphisme. Pour cela il aurait fallut avoir un système de profondeur que normalement Tkinter gère.

Cela aurait aussi permis de pallier le problème de d'effaçage du dessin à cause du rectangle rouge de la zone de dessin. En effet lorsque l'on change le zoom où la localisation de la zone de dessin, le précédent rectangle est 'effacé' par un dessin du même rectangle mais en blanc par dessus. Cela efface alors aussi les traits de cayons dessinés auparavant :

![Effacage](./images/effacage.png)

Une autre chose à améliorer serait les delais d'envois. En effet le système est un peu lent ce qui empêche la fluidité du dessin.

## Conclusion générale

Ce TP m'aura en tout cas permise de remobiliser d'une part les connaissances acquises sur la programmation de la STM32f7-discovery (acquisition via l'ADC, l'écran tactile et liaison série) et d'autre part les concepts liés à la programmation objet, un outil indispensable lorsqu'on a une utilisation intéractive.

![smiley](./images/smiley.PNG)
